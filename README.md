# llama.any 
A cross-platform CLI client for the llama.any protocol

## Building
This CMake project builds using the Nintendo DS and Nintendo 3DS devkitpro toolchains or on both Linux and Windows. More platforms may easily be implemented at `Runtime.{hpp,cpp}`.

## Server
The server repo is located at: https://gitlab.com/niansa/llama_any_server!
