#ifndef _SENDER_HPP
#define _SENDER_HPP
#include "AsyncManager.hpp"

#include <string_view>
#include <vector>
#include <cstddef>
#include "basic-coro/AwaitableTask.hpp"


namespace Sender {
class Simple {
    AsyncManager &aMan;

protected:
    int fd;

public:
    Simple(AsyncManager& asyncManager, int fd) : aMan(asyncManager), fd(fd) {}

    basiccoro::AwaitableTask<AsyncResult> write(std::string_view, bool moreData = false);
    basiccoro::AwaitableTask<AsyncResult> write(const std::byte *data, size_t, bool moreData = false);

    template<typename T>
    auto writeObject(const T& o, bool moreData = false) {
        return write(reinterpret_cast<const std::byte *>(&o), sizeof(o), moreData);
    }
};
}
#endif
