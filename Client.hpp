#ifndef CLIENT_HPP
#define CLIENT_HPP
#include "Runtime.hpp"
#include "AsyncManager.hpp"
#include "Socket.hpp"
#include "Sender.hpp"
#include "Receiver.hpp"
#include "basic-coro/AwaitableTask.hpp"

#include <string>
#include <string_view>
#include <stdexcept>
#include <memory>
#include <functional>
#ifndef PLATFORM_WINDOWS
#   include <netdb.h>
#else
#   include <ws2tcpip.h>
#endif


class Client
{
    struct Exception : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    AsyncManager& aMan;

    int fd = -1;
#   ifdef HAS_ADDRINFO
    addrinfo
#   else
    bool sent = false;
    hostent
#   endif
             *addrInfo; // Can't be null unless request has already been sent

    std::unique_ptr<SocketConnection<Sender::Simple, Receiver::Simple>> connection;

    void fetchAddr(const std::string& addr, unsigned port);

public:
    Client(const std::string &addr, unsigned port, AsyncManager& asyncManager);

    basiccoro::AwaitableTask<AsyncResult> ask(std::string_view prompt, const std::function<basiccoro::AwaitableTask<void> (unsigned progress)>& on_progress, const std::function<basiccoro::AwaitableTask<void> (std::string_view token)>& on_token);
};

#endif // CLIENT_HPP
