#ifndef _RECEIVER_HPP
#define _RECEIVER_HPP
#include "Runtime.hpp"
#include "AsyncManager.hpp"

#include <string>
#include <cstddef>
#include "basic-coro/AwaitableTask.hpp"


namespace Receiver {
class Simple {
    Runtime& runtime;
    AsyncManager &aMan;

protected:
    int fd;

public:
    Simple(AsyncManager& asyncManager, int fd) : runtime(asyncManager.getRuntime()), aMan(asyncManager), fd(fd) {}

    // Reads the exact amount of bytes given
    basiccoro::AwaitableTask<std::string> read(size_t amount);
    basiccoro::AwaitableTask<AsyncResult> read(std::byte *buffer, size_t size);
    // Reads at max. the amount of bytes given
    basiccoro::AwaitableTask<std::string> readSome(size_t max);

    // Reads an object of type T
    template<typename T>
    auto readObject(T& o) {
        return read(reinterpret_cast<std::byte *>(&o), sizeof(o));
    }
};
}
#endif
