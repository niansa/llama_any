#include "Runtime.hpp"
#include "Sender.hpp"
#include "basic-coro/AwaitableTask.hpp"

#include <string_view>
#ifndef PLATFORM_WINDOWS
#   include <sys/socket.h>
#   include <sys/select.h>
#else
#   include <ws2tcpip.h>
#   include <winsock2.h>
#endif



basiccoro::AwaitableTask<AsyncResult> Sender::Simple::write(std::string_view str, bool moreData) {
    co_return co_await this->write(reinterpret_cast<const std::byte*>(str.data()), str.size(), moreData);
}

basiccoro::AwaitableTask<AsyncResult> Sender::Simple::write(const std::byte *data, size_t size, bool moreData) {
    std::string fres;

    // Wait for socket to get ready for writing
    if (co_await aMan.waitWrite(fd) == AsyncResult::Error) [[unlikely]] {
        co_return AsyncResult::Error;
    }

    // Write
    co_return (send(fd, reinterpret_cast<const char*>(data), size, MSG_FLAGS_OR_ZERO(MSG_NOSIGNAL | (int(moreData)*MSG_MORE))) < 0)?AsyncResult::Error:AsyncResult::Success;
}
