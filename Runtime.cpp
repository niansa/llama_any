#include "Runtime.hpp"

#ifdef PLATFORM_3DS
#include <string>
#include <exception>

#include <cerrno>
#include <3ds.h>



void Runtime::customTerminate() noexcept {
    // Get error message
    std::string message;
    try {
        std::rethrow_exception(std::current_exception());
    } catch (const std::exception& e) {
        message = e.what();
    } catch (...) {
        message = "Unknown";
    }
    // Display error
    errorConf conf = {
        .type = ERROR_TEXT_WORD_WRAP,
        .errorCode = errno,
        .upperScreenFlag = ERROR_NORMAL,
        .useLanguage = CFG_LANGUAGE_EN,
        .Text = {L'I', L'N', L'V', L'A', L'L', L'I', L'D', L'\0'},
        .homeButton = true,
        .softwareReset = false,
        .appJump = false,
        .returnCode = ERROR_UNKNOWN,
        .eulaVersion = 0
    };
    errorText(&conf, ("An exception was thrown but never handled:\n\n"+message).c_str());
    errorDisp(&conf);
    // Exit
    aptExit();
    socExit();
    gfxExit();
    exit(-errno);
}
#elif PLATFORM_DS
void Runtime::kbCallback(int key) {
    if (key > 0) printf("%c", key);
}
#endif
