#include "Receiver.hpp"

#include <string_view>
#include <array>
#ifndef PLATFORM_WINDOWS
#   include <sys/socket.h>
#   include <sys/select.h>
#else
#   include <ws2tcpip.h>
#endif



basiccoro::AwaitableTask<std::string> Receiver::Simple::read(size_t amount) {
    // Create buffer
    std::string fres;
    fres.resize(amount);

    // Read into buffer
    co_await read(reinterpret_cast<std::byte*>(fres.data()), fres.size());

    // Return final buffer
    co_return fres;
}
basiccoro::AwaitableTask<AsyncResult> Receiver::Simple::read(std::byte *buffer, size_t size) {
    size_t allBytesRead = 0;

    while (allBytesRead != size) {
        // Wait for data
        if (co_await aMan.waitRead(fd) == AsyncResult::Error) [[unlikely]] {
            // Error
            co_return AsyncResult::Error;
        }

        // Receive data
        ssize_t bytesRead;
        if ((bytesRead = recv(fd, reinterpret_cast<char*>(buffer+allBytesRead), size-allBytesRead, 0)) < 0) [[unlikely]] {
            // Error
            co_return AsyncResult::Error;
        }
        allBytesRead += bytesRead;
    }

    // No error
    co_return AsyncResult::Success;
}

basiccoro::AwaitableTask<std::string> Receiver::Simple::readSome(size_t max) {
    // Create buffer
    std::string fres;
    fres.resize(max);

    // Wait for data
    if (co_await aMan.waitRead(fd) == AsyncResult::Error) [[unlikely]] {
        co_return "";
    }

    // Receive data
    ssize_t bytesRead;
    if ((bytesRead = recv(fd, fres.data(), max, 0)) < 0) [[unlikely]] {
        co_return "";
    }

    // Resize and return final buffer
    fres.resize(bytesRead);
    co_return fres;
}
