#ifndef _ASYNCMANAGER_HPP
#define _ASYNCMANAGER_HPP
#include "Runtime.hpp"

#include <unordered_map>
#include <memory>
#include "basic-coro/AwaitableTask.hpp"
#include "basic-coro/SingleEvent.hpp"

class Runtime;



enum class AsyncResult {
    Error,
    Success
};

class AsyncManager {
public:
    using SockFuture = basiccoro::SingleEvent<AsyncResult>;
    using SockFutureUnique = std::unique_ptr<SockFuture>;
    using SockFutureMap = std::unordered_multimap<int, SockFutureUnique>;

private:
    Runtime& runtime;

    SockFutureMap sockReads;
    SockFutureMap sockWrites;
    bool stopping = false;

    static
    void cleanFutureMap(SockFutureMap&);

public:
    AsyncManager(Runtime& runtime) : runtime(runtime) {}
    AsyncManager(AsyncManager&) = delete;
    AsyncManager(const AsyncManager&) = delete;
    AsyncManager(AsyncManager&&) = delete;

    void run();
    void stop() {
        stopping = true;
    }

    basiccoro::AwaitableTask<AsyncResult> waitRead(int fd) {
        auto event = std::make_unique<SockFuture>();
        auto eventPtr = event.get();
        sockReads.emplace(fd, std::move(event));
        co_return co_await *eventPtr;
    }
    basiccoro::AwaitableTask<AsyncResult> waitWrite(int fd) {
        auto event = std::make_unique<SockFuture>();
        auto eventPtr = event.get();
        sockWrites.emplace(fd, std::move(event));
        co_return co_await *eventPtr;
    }

    auto& getRuntime() const {
        return runtime;
    }
};
#endif
